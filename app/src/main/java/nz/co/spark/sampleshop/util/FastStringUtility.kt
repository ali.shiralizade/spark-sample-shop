package nz.co.spark.sampleshop.util

import android.content.Context
import android.util.DisplayMetrics

object FastStringUtility {
    /**
     * This method converts all arabic and persian numerals to english numerals
     *
     *
     * For faster execution I choose not to use regex and pattern matcher to keep
     * execution time around O(n)
     *
     * @param input a CharSequence we need its numbers converted
     * @return a String with converted numbers
     */
    fun convertNumeralsToEnglish(input: CharSequence?): String? {
        if (input == null) return null
        val sb = StringBuilder(input.length)
        for (element in input) {
            var ch = element
            when (ch) {
                '١', '۱' -> ch = '1'
                '٢', '۲' -> ch = '2'
                '٣', '۳' -> ch = '3'
                '٤', '۴' -> ch = '4'
                '٥', '۵' -> ch = '5'
                '٦', '۶' -> ch = '6'
                '٧', '۷' -> ch = '7'
                '٨', '۸' -> ch = '8'
                '٩', '۹' -> ch = '9'
                '٠', '۰' -> ch = '0'
            }
            sb.append(ch)
        }
        return sb.toString()
    }

    /**
     * This method strip all non numerals characters
     *
     * **it also keeps arabic and persian numerals**
     * Based on tests simple version is 12 times more faster than regex version
     * @param input A String which it's non numerals are going to be removed
     * @return A String containing just numerals e.g. "a۲5ش" will turn to "۲5"
     */
    fun stripNonNumerals(input: String?): String? {
        if (input == null) return null
        val sb = StringBuilder(input.length)
        for (element in input) {
            if (isNumeral(element)) {
                sb.append(element)
            }
        }
        return sb.toString()
    }

    fun stripNonNumeralsRegex(input: String?): String? {
        return input?.replace("[^\\d٠-٩۰-۹]".toRegex(), "")
    }

    fun getNumeralsCountBeforePosition(str: CharSequence, pos: Int): Int {
        var count = 0
        var i = 0
        while (i < str.length && i < pos) {
            val ch = str[i]
            if (isNumeral(ch)) {
                count++
            }
            i++
        }
        return count
    }

    fun isNumeral(ch: Char): Boolean {
        return ch in '0'..'9' || ch in '٠'..'٩' || ch in '۰'..'۹'
    }

    fun getCursorPositionForNumeralAt(str: CharSequence, positionOnNumeral: Int): Int {
        var count = 0
        for (i in str.indices) {
            val ch = str[i]
            if (isNumeral(ch)) {
                count++
                if (count == positionOnNumeral) {
                    return i + 1
                }
            }
        }
        return str.length
    }

    fun replaceCharAt(str: String, replacement: Char, index: Int): String {
        val chars = str.toCharArray()
        chars[index] = replacement
        return String(chars)
    }

    fun getDeviceDensityString(context: Context): String {
        when (context.resources.displayMetrics.densityDpi) {
            DisplayMetrics.DENSITY_LOW -> return "ldpi"
            DisplayMetrics.DENSITY_MEDIUM -> return "mdpi"
            DisplayMetrics.DENSITY_TV, DisplayMetrics.DENSITY_HIGH -> return "hdpi"
            DisplayMetrics.DENSITY_260, DisplayMetrics.DENSITY_280, DisplayMetrics.DENSITY_300, DisplayMetrics.DENSITY_XHIGH -> return "xhdpi"
            DisplayMetrics.DENSITY_340, DisplayMetrics.DENSITY_360, DisplayMetrics.DENSITY_400, DisplayMetrics.DENSITY_420, DisplayMetrics.DENSITY_440, DisplayMetrics.DENSITY_XXHIGH -> return "xxhdpi"
            DisplayMetrics.DENSITY_560, DisplayMetrics.DENSITY_XXXHIGH -> return "xxxhdpi"
        }
        return "unknown"
    }
}