package nz.co.spark.sampleshop.app

import android.app.Activity
import android.app.Application
import android.os.Bundle
import java.lang.ref.WeakReference

fun<T> weak(a: T) = WeakReference(a)

object AppActivityLifecycleCallback : Application.ActivityLifecycleCallbacks {
    @Suppress("MemberVisibilityCanBePrivate")
    var currentActivity: WeakReference<Activity>? = null


    override fun onActivityCreated(activity: Activity, bundle: Bundle?) {
        currentActivity = weak(activity)
    }
    override fun onActivityStarted(activity: Activity) {
        currentActivity = weak(activity)
    }
    override fun onActivityResumed(activity: Activity) {
        currentActivity = weak(activity)
    }

    override fun onActivityPaused(activity: Activity) {
        currentActivity = weak(activity)
    }
    override fun onActivityStopped(activity: Activity) {
    }
    override fun onActivityDestroyed(activity: Activity) {
    }

    override fun onActivitySaveInstanceState(activity: Activity, bundle: Bundle) {
    }



}