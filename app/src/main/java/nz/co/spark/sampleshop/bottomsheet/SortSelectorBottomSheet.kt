package nz.co.spark.sampleshop.bottomsheet

import android.os.Bundle
import androidx.fragment.app.activityViewModels
import nz.co.spark.sampleshop.R
import nz.co.spark.sampleshop.adapter.FiltersAdapters
import nz.co.spark.sampleshop.databinding.BottomsheetSortSelectorBinding
import nz.co.spark.sampleshop.viewmodel.ProductListSharedViewModel

class SortSelectorBottomSheet : BaseBottomSheet<BottomsheetSortSelectorBinding, ProductListSharedViewModel>() {
    override fun layout() = R.layout.bottomsheet_sort_selector
    override val viewModel: ProductListSharedViewModel by activityViewModels()

    val filtersAdapter = FiltersAdapters{
        viewModel.sort.value = it
        dismiss()
    }

    override fun onBindingCreated(savedInstanceState: Bundle?) {
        binding.recyclerView.apply {
            adapter = filtersAdapter
        }
    }
}