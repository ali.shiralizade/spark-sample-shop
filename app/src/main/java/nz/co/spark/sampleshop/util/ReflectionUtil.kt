package nz.co.spark.sampleshop.util

import java.lang.reflect.Field

val Class<*>.allFields: ArrayList<out Field> get() {
    var cls : Class<*>? = this
    val ans = ArrayList<Field>()
    while (cls != null && cls != Any::class.java){
        ans.addAll(cls.declaredFields)
        cls = cls.superclass
    }
    return ans
}

inline fun<reified T> List<T>.array() = Array(size) { get(it) }