package nz.co.spark.sampleshop.extensions

import android.view.View
import androidx.fragment.app.Fragment
import nz.co.spark.sampleshop.util.KeyboardUtility

fun View.hideKeyboard() = KeyboardUtility.hideKeyboard(context, this)
fun View.showKeyboard() = KeyboardUtility.showKeyboard(context, this)

fun Fragment.hideKeyboard() = context?.let { context ->
    view?.let { view ->
        KeyboardUtility.hideKeyboard(context, view)
    }
}

fun Fragment.showKeyboard() = context?.let { context ->
    view?.let { view ->
        KeyboardUtility.showKeyboard(context, view)
    }
}