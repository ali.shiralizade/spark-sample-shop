package nz.co.spark.sampleshop.util

object PermissionsAutoIncrement {
    private const val minVal = 10_000
    private const val maxVal = 64_999
    private const val diff = maxVal-minVal

    private var value = minVal
    operator fun invoke() : Int{
        value = (value - minVal + 1) % diff + minVal
        return value
    }
}