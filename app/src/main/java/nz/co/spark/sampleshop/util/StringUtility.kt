package nz.co.spark.sampleshop.util

import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

object StringUtility {
    private const val format = "#,###"
    private const val decimalFormat = "#,###.##"

    fun isNotEmpty(charSequence: CharSequence?): Boolean {
        return charSequence != null && !isEmpty(charSequence)
    }

    fun isEmpty(charSequence: CharSequence?): Boolean {
        return charSequence == null || charSequence.toString().trim { it <= ' ' }.isEmpty()
    }

    fun formatDouble(
        d: Double,
        isDecimal: Boolean = true,
        locale: Locale = Locale.US
    ): String? {
        val nf = NumberFormat.getNumberInstance(locale)
        val df = nf as DecimalFormat
        if (isDecimal) {
            df.applyPattern(decimalFormat)
        } else {
            df.applyPattern(format)
        }
        return df.format(d)
    }


    fun formatLong(l: Long, locale: Locale = Locale.US): String? {
        val nf = NumberFormat.getNumberInstance(locale)
        val df = nf as DecimalFormat
        df.applyPattern(format)
        return df.format(l)
    }

    fun formatInteger(i: Int?, locale: Locale = Locale.US): String? {
        val nf = NumberFormat.getNumberInstance(locale)
        val df = nf as DecimalFormat
        df.applyPattern(format)
        return df.format(i)
    }


    fun capitalize(s: String?): String? {
        return if (s != null && s.isNotEmpty()) {
            val first = s[0]
            if (Character.isUpperCase(first)) s else Character.toUpperCase(
                first
            ).toString() + s.substring(1)
        } else {
            ""
        }
    }
}