package nz.co.spark.sampleshop.api;

import com.google.gson.annotations.SerializedName;

public class ApiResponseWrapper<T> {
    @SerializedName("success")
    public boolean success;

    @SerializedName("data")
    public T data;

    @SerializedName("error")
    public String errorMessage;

}
