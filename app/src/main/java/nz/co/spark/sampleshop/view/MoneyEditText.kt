package nz.co.spark.sampleshop.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.util.AttributeSet
import android.util.TypedValue
import android.view.ContextThemeWrapper
import android.view.Gravity
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.text.TextUtilsCompat
import androidx.core.view.ViewCompat
import nz.co.spark.sampleshop.R
import nz.co.spark.sampleshop.util.FastStringUtility.convertNumeralsToEnglish
import nz.co.spark.sampleshop.util.FastStringUtility.getCursorPositionForNumeralAt
import nz.co.spark.sampleshop.util.FastStringUtility.getNumeralsCountBeforePosition
import nz.co.spark.sampleshop.util.FastStringUtility.isNumeral
import nz.co.spark.sampleshop.util.FastStringUtility.replaceCharAt
import nz.co.spark.sampleshop.util.FastStringUtility.stripNonNumerals
import nz.co.spark.sampleshop.util.StringUtility.formatLong
import java.util.*
import kotlin.math.abs
import kotlin.math.max

class MoneyEditText : AppCompatEditText {
    interface MoneyAmountChangedListener {
        fun onMoneyAmountChanged(amount: Long)
    }

    var currencyUnit: String? = null
    var currencyPadding = 0
    var centerWholeText = false
    var moneyAmountChangedListener: MoneyAmountChangedListener? = null

    constructor(context: Context?) : super(
        ContextThemeWrapper(
            context,
            R.style.EditText_Charge
        )
    ) {
        init(null)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        ContextThemeWrapper(context, R.style.EditText_Charge),
        attrs
    ) {
        init(attrs)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(
        ContextThemeWrapper(context, R.style.EditText_Charge),
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.MoneyEditText)
        currencyUnit = a.getString(R.styleable.MoneyEditText_sb_met_currency)
        if (currencyUnit == null) {
            currencyUnit = "ریال"
        }
        currencyPadding =
            a.getDimensionPixelSize(R.styleable.MoneyEditText_sb_met_currency_padding, spToPx(8f))
        centerWholeText = a.getInt(R.styleable.MoneyEditText_sb_met_center_based_on, 0) == 1
        a.recycle()
        addTextChangedListener(currencyAndFormatTextWatcher)
        inputType = InputType.TYPE_CLASS_NUMBER
        layoutDirection = View.LAYOUT_DIRECTION_LTR
        textDirection = View.TEXT_DIRECTION_LTR
        textAlignment = View.TEXT_ALIGNMENT_CENTER
        gravity = Gravity.CENTER
        background = null
    }

    private val currencyAndFormatTextWatcher: TextWatcher = object : TextWatcher {
        @Synchronized
        private fun runSilently(r: Runnable) {
            removeTextChangedListener(this)
            r.run()
            addTextChangedListener(this)
        }

        var pointerPositionOnText = 0
        var replacementChar = -1
        var targetCursorPosition = -1
        var result: String? = ""
        override fun beforeTextChanged(
            s: CharSequence,
            start: Int,
            count: Int,
            after: Int
        ) {
            runSilently(Runnable {
                result = null
                var checkPosition = start
                replacementChar = -1
                if (count == 1 && after == 0 && !isNumeral(s[start])) {
                    checkPosition = start - 1
                    replacementChar = start - 1
                }
                pointerPositionOnText =
                    getNumeralsCountBeforePosition(s, checkPosition)
            })
        }

        override fun onTextChanged(
            s: CharSequence,
            start: Int,
            before: Int,
            count: Int
        ) {
            runSilently(Runnable {
                if (text == null) return@Runnable
                result = text.toString()
                if (replacementChar >= 0 && replacementChar < result!!.length) {
                    result = replaceCharAt(result!!, ' ', replacementChar)
                }
                val amount = getAmount(result)
                result = if (amount > 0) {
                    getFormattedText(amount)
                } else {
                    ""
                }
                targetCursorPosition = getCursorPositionForNumeralAt(
                    result!!,
                    pointerPositionOnText + count
                )
            })
        }

        override fun afterTextChanged(s: Editable) {
            runSilently(Runnable {
                if (result != null) {
                    val realFilters = s.filters
                    if (realFilters != null) {
                        val filters =
                            ArrayList<InputFilter>()
                        for (f in realFilters) {
                            if (f !is DigitsKeyListener) {
                                filters.add(f)
                            }
                        }
                        var tempFilters: Array<InputFilter?> =
                            arrayOfNulls(filters.size)
                        tempFilters = filters.toArray(tempFilters)
                        s.filters = tempFilters
                    }
                    s.replace(0, s.length, result)
                    if (realFilters != null) {
                        s.filters = realFilters
                    }
                    if (targetCursorPosition >= 0 && targetCursorPosition < s.length) setSelection(
                        targetCursorPosition
                    )
                    sendAmountChangedEvent()
                }
            })
        }
    }

    override fun setText(text: CharSequence, type: BufferType) {
        super.setText(text, type)
        sendAmountChangedEvent()
    }

    fun getFormattedText(amount: Long): String? {
        return formatLong(max(0, amount)) //due to tests DecimalFormatter is 4 times faster than String.format(getTextLocale(), "%,d", amount);
    }

    fun changeAmountBy(amount: Long) {
        this.amount = this.amount + amount
    }

    var amount: Long
        get() = if (text != null) getAmount(text.toString()) else 0
        set(amount) {
            setText(getFormattedText(amount))
        }

    fun getAmount(str: String?): Long {
        if (str != null) {
            // strip non numerals from remaining text (do it first to reduce the cost of convertNumeralsToEnglish)
            var text = stripNonNumerals(str)

            // convert non English numerals no English numerals
            text = convertNumeralsToEnglish(text)
            try {
                if (text != null) {
                    return max(0, text.toLong())
                }
            } catch (ignored: Exception) {
            }
        }
        return 0
    }

    val boundsForText = Rect()
    val boundsForCurrency = Rect()
    var paint: Paint? = null
    var defaultPaintColor = 0

    //XXX: For getScrollX() refer to https://stackoverflow.com/a/30350214
    override fun onDraw(canvas: Canvas) {
        if (text != null && text!!.isNotEmpty() && currencyUnit != null) {
            val isCurrencyOnLeft = isCurrencyOnLeft
            val viewOffsetX = viewOffsetX()
            val viewPadding = abs(viewOffsetX)
            if (isCurrencyOnLeft) {
                setPadding(viewPadding, 0, 0, 0)
            } else {
                setPadding(0, 0, viewPadding, 0)
            }
            paint = getPaint() //you can change it to something else if required
            paint?.getTextBounds(text.toString(), 0, text!!.length, boundsForText)
            val viewW = measuredWidth
            val viewH = measuredHeight
            val textW = boundsForText.width()
            paint?.getTextBounds(currencyUnit, 0, currencyUnit!!.length, boundsForCurrency)
            val cW = boundsForCurrency.width()
            val cH = boundsForCurrency.height()
            canvas.save()
            //            canvas.translate(viewOffsetX, 0);
            super.onDraw(canvas)
            run {
                canvas.save()
                val drawLeft =
                    if (isCurrencyOnLeft) (viewW - textW) / 2f - (cW + currencyPadding) + viewPadding / 2f else (viewW + textW) / 2f + currencyPadding - viewPadding / 2f
                canvas.translate(scrollX + drawLeft, viewH / 2f + cH / 3f)
                canvas.drawText(currencyUnit!!, 0f, 0f, paint!!)
                canvas.restore()
            }
            canvas.restore()
        } else {
            if (getHint() != null && getHint().isNotEmpty()) {
                hint = getHint().toString()
                setHint("")
            }
            paint = getPaint() //you can change it to something else if required
            paint?.let { paint ->
                paint.getTextBounds(hint, 0, hint.length, boundsForText)
                val viewW = measuredWidth
                val viewH = measuredHeight
                val textW = boundsForText.width()
                setPadding(0, 0, 0, 0)
                defaultPaintColor = paint.color
                paint.color = currentHintTextColor
                canvas.save()
                val drawLeft = (viewW - textW) / 2f
                canvas.translate(scrollX + drawLeft, viewH / 2f)
                canvas.drawText(hint, 0f, 0f, paint)
                canvas.restore()
                paint.color = defaultPaintColor
            }

            super.onDraw(canvas)
        }
    }

    private var hint = ""
    private val isCurrencyOnLeft: Boolean
        get() = TextUtilsCompat.getLayoutDirectionFromLocale(textLocale) == ViewCompat.LAYOUT_DIRECTION_RTL

    private fun viewOffsetX(): Int {
        if (centerWholeText && text != null && text!!.isNotEmpty() && currencyUnit != null) {
            paint = getPaint() //you can change it to something else if required
            paint?.getTextBounds(currencyUnit, 0, currencyUnit!!.length, boundsForCurrency)
            val additionalWidthDueToCurrency =
                boundsForCurrency.width() + currencyPadding // /2;
            return if (isCurrencyOnLeft) additionalWidthDueToCurrency else -additionalWidthDueToCurrency
        }
        return 0
    }

    fun spToPx(sp: Float): Int {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP,
            sp,
            context.resources.displayMetrics
        ).toInt()
    }

    private fun sendAmountChangedEvent() {
        if (moneyAmountChangedListener != null) {
            moneyAmountChangedListener!!.onMoneyAmountChanged(amount)
        }
    }
}