package nz.co.spark.sampleshop.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.view.ContextThemeWrapper
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import nz.co.spark.sampleshop.R
import nz.co.spark.sampleshop.data.model.Resource
import nz.co.spark.sampleshop.extensions.getCustomErrorMessage
import nz.co.spark.sampleshop.extensions.setView
import nz.co.spark.sampleshop.extensions.setVm
import nz.co.spark.sampleshop.util.UiHelper
import nz.co.spark.sampleshop.viewmodel.BaseViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel


abstract class BaseFragment<B : ViewDataBinding, VM: BaseViewModel> : Fragment() ,
    CoroutineScope by CoroutineScope(Dispatchers.Main)
{
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner,object: OnBackPressedCallback(navController?.previousBackStackEntry != null){
            override fun handleOnBackPressed() {
                navigateUp()
            }
        });
    }

    @LayoutRes abstract fun layout(): Int
    protected abstract val viewModel: VM

    protected lateinit var binding: B

    open fun canHandleBack() = false
    open fun onBackPressed(){}



    @CallSuper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        val contextThemeWrapper = ContextThemeWrapper(activity, R.style.AppTheme)
        val localInflater = inflater.cloneInContext(contextThemeWrapper)


        binding = DataBindingUtil.inflate( localInflater, layout(), container, false )

        binding.lifecycleOwner = this

        binding.setView(this)
        binding.setVm(viewModel)
        onBindingCreated(savedInstanceState)
        registerObservers()

        return binding.root
    }

    open fun registerObservers(){}

    open fun onBindingCreated(savedInstanceState: Bundle?){}

    override fun onDestroy() {
        super.onDestroy()
        cancel()
    }



    protected val navController: NavController? get() {
        return try {
            findNavController()
        }catch (e: Exception){
            e.printStackTrace()
            null
        }
    }

    open fun navigateUp() = navController?.navigateUp() ?: false


    private var latestSnackBar: Snackbar? = null
    fun Resource<*>?.showErrorSnackBar(dismissPrevious : Boolean = true): Snackbar? {
        if (this == null) return null
        if(dismissPrevious){
            latestSnackBar?.dismiss()
        }
        latestSnackBar = UiHelper.showErrorSnackBar(binding.root, errorObject.getCustomErrorMessage())
        return latestSnackBar
    }
    fun hideSnackBar() = latestSnackBar?.dismiss()

    fun Resource<*>?.hideOrShowSnackBar() {
        if (this?.isError() == true) {
            showErrorSnackBar()
        } else {
            hideSnackBar()
        }
    }





    fun navigate(destination: NavDirections, options: NavOptions? = null) = with(findNavController()) {
        currentDestination?.getAction(destination.actionId)
            ?.let { navigate(destination,options) }
    }


    fun showSnackBar(message: String):Snackbar{
        return UiHelper.showErrorSnackBar(binding.root,message )
    }


}


//inline fun<T> Fragment.observe(liveData: LiveData<T>?, crossinline observer: ((T)->Unit)) {
fun<T> Fragment.observe(liveData: LiveData<T>?, observer: ((T)->Unit)) {
    liveData?.observe(viewLifecycleOwner, Observer { observer(it) })
}
fun<T> Fragment.observeNullable(liveData: LiveData<T>?, observer: ((T?)->Unit)) {
    liveData?.observe(viewLifecycleOwner, Observer { observer(it) })
}

inline fun Fragment.withFragmentManager(crossinline action: (FragmentManager)->Unit) {
    if (isAdded){
        action(parentFragmentManager)
    }
}