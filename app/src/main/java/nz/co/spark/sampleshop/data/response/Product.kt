package nz.co.spark.sampleshop.data.response

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import kotlinx.android.parcel.Parcelize
import com.google.gson.annotations.SerializedName as SN

@Parcelize
data class Product(
    @SN("id")
    val id: String?,

    @SN("name")
    val name: String?,

    @SN("manufacturer")
    val manufacturer: String?,

    @SN("image")
    val image: String?,

    @SN("price")
    val price: String?,

    @SN("price_formated")
    val priceFormated: String?

) : Parcelable


class ProductDiffUtilCallback : DiffUtil.ItemCallback<Product?>() {
    override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
        return  oldItem.id == newItem.id
                && oldItem.name == newItem.name
                && oldItem.image == newItem.image
                && oldItem.price == newItem.price
    }

}