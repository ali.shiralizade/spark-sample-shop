package nz.co.spark.sampleshop.api;

public class ApiFailedException extends RuntimeException{
    public ApiFailedException(String message) {
        super(message);
    }
}