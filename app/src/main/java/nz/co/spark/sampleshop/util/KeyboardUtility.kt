package nz.co.spark.sampleshop.util

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

object KeyboardUtility  {

    fun showKeyboard(context: Context, v: View) {
        val imm =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        if (imm != null) {
            v.requestFocus()
            imm.showSoftInput(v, 0)
        }
    }

    fun hideKeyboard(context: Context, v: View) {
        val imm =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(v.windowToken, 0)
    }

    fun tryHideKeyboard(
        context: Context,
        v: View
    ): Boolean {
        return try {
            val imm =
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm != null && imm.hideSoftInputFromWindow(v.windowToken, 0)
        } catch (var3: Exception) {
            var3.printStackTrace()
            false
        }
    }
}