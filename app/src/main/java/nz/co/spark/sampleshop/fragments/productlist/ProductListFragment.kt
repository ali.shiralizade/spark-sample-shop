package nz.co.spark.sampleshop.fragments.productlist

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import nz.co.spark.sampleshop.R
import nz.co.spark.sampleshop.adapter.ProductListAdapter
import nz.co.spark.sampleshop.bottomsheet.SortSelectorBottomSheet
import nz.co.spark.sampleshop.data.model.Status
import nz.co.spark.sampleshop.data.response.SortType
import nz.co.spark.sampleshop.databinding.BottomsheetSortSelectorBinding
import nz.co.spark.sampleshop.databinding.FragmentProductListBinding
import nz.co.spark.sampleshop.fragments.BaseFragment
import nz.co.spark.sampleshop.fragments.observe
import nz.co.spark.sampleshop.viewmodel.ProductListSharedViewModel


class ProductListFragment : BaseFragment<FragmentProductListBinding, ProductListFragmentVM>() {
    override val viewModel: ProductListFragmentVM by viewModels()
    override fun layout() = R.layout.fragment_product_list

    val sharedViewModel: ProductListSharedViewModel by activityViewModels()

    private val listAdapter = ProductListAdapter()

    val args: ProductListFragmentArgs by navArgs()
    val category get() = args.category

    override fun onBindingCreated(savedInstanceState: Bundle?) {
        binding.recyclerView.apply {
            adapter = listAdapter
        }
        viewModel.category.value = category
    }

    override fun registerObservers() {

        observe(viewModel.productsLiveData) {
            listAdapter.submitList(it)
            binding.showError = false
        }

        observe(viewModel.networkState) {
            binding.showError = it == Status.ERROR && listAdapter.itemCount == 0
        }

        observe(sharedViewModel.sort){
            viewModel.sort.value = it
        }
    }

    fun openSort(){
        SortSelectorBottomSheet().show(parentFragmentManager)
    }

}