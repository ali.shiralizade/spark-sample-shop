package nz.co.spark.sampleshop.data.response
import com.google.gson.annotations.SerializedName as SN

data class SessionResponse(
    @SN("session")
    var session: String
)