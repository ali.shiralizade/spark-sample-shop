package nz.co.spark.sampleshop.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import nz.co.spark.sampleshop.data.response.Product
import nz.co.spark.sampleshop.data.response.ProductDiffUtilCallback
import nz.co.spark.sampleshop.databinding.ItemProductListBinding

class ProductListAdapter : PagedListAdapter<Product, ProductListViewHolder>(ProductDiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = ItemProductListBinding.inflate(inflater)
        return ProductListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductListViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }
}


class ProductListViewHolder(val binding: ItemProductListBinding) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.view = this
    }
    fun bind(product: Product) {
        binding.product = product
    }

}