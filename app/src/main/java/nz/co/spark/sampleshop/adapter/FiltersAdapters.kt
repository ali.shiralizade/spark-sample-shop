package nz.co.spark.sampleshop.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import nz.co.spark.sampleshop.data.response.Category
import nz.co.spark.sampleshop.data.response.SortType
import nz.co.spark.sampleshop.databinding.ItemFilterBinding

class FiltersAdapters(val onClickListener: (SortType) -> Unit) : RecyclerView.Adapter<FilterViewHolder>() {

    val values = SortType.values()

    override fun getItemCount() = values.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = ItemFilterBinding.inflate(inflater)
        return FilterViewHolder(view, onClickListener)
    }

    override fun onBindViewHolder(holder: FilterViewHolder, position: Int) {
        holder.bind(values[position])
    }
}

class FilterViewHolder(val binding: ItemFilterBinding, val onClickListener: (SortType) -> Unit) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.view = this
    }
    fun bind(sortType: SortType) {
        binding.sortType = sortType
    }

    fun onClick(sortType: SortType){
        onClickListener(sortType)
    }

}