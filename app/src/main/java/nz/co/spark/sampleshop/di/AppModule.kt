package nz.co.spark.sampleshop.di

import nz.co.spark.sampleshop.app.App
import nz.co.spark.sampleshop.util.AppPreferences
import org.koin.android.ext.koin.androidApplication
import org.koin.core.context.GlobalContext
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier
import org.koin.dsl.module

val appModule = module {
    single { AppPreferences(get()) }
    single { androidApplication() as App }
}



inline fun <reified T : Any> inject(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null
) = lazy { GlobalContext.get().koin.get<T>(qualifier, parameters) }
