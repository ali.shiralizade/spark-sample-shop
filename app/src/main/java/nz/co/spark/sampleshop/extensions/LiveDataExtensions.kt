package nz.co.spark.sampleshop.extensions

import android.os.Looper
import androidx.annotation.UiThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import nz.co.spark.sampleshop.util.SingleLiveEvent
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.experimental.ExperimentalTypeInference
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty


fun <T> MutableLiveData<T>.emit(t: T? = null) {
    setOrPostValue(t)
}
fun <T> MutableLiveData<T>.setOrPostValue(t: T? = null) {
    if (Looper.myLooper() == Looper.getMainLooper()) {
        value = t
    } else {
        postValue(t)
    }
}

fun <T> LiveData<T>.toLiveEvent(): SingleLiveEvent<T> {
    val result = SingleLiveEvent<T>()
    result.addSource(this) {
        result.value = it
    }
    return result
}

fun <T> LiveData<T>.toVoidLiveEvent(): SingleLiveEvent<Void> {
    val result = SingleLiveEvent<Void>()
    result.addSource(this) {
        result.value = null
    }
    return result
}

fun <T> LiveData<T?>.unwrapNull(defaultVal: T, preFill: T? = null): LiveData<T> {
    val result = MediatorLiveData<T>()
    preFill?.let { result.value = it }
    result.addSource(this) {
        result.value = it ?: defaultVal
    }
    return result
}

fun LiveData<Boolean?>.nullToFalse(): LiveData<Boolean> {
    return unwrapNull(defaultVal = false, preFill = false)
}


fun <T> LiveData<T>.observeOnceFor(timeout: Long, onFailCallback : (()->Unit)? = null, observer: ((T) -> Unit)) {
    class SingleObserver : Observer<T> {
        var removed = AtomicBoolean(false)
        override fun onChanged(t: T) {
            observer(t)
            removeMe()
        }

        @Synchronized
        @UiThread
        fun removeMe() : Boolean {
            if (!removed.getAndSet(true)) {
                removeObserver(this@SingleObserver)
                return true
            }
            return false
        }
    }

    val obs = SingleObserver()
    observeForever(obs)
    GlobalScope.launch(Dispatchers.Main) {
        delay(timeout)
        if(obs.removeMe() && onFailCallback != null){
            onFailCallback()
        }
    }

}

fun <T> LiveData<T>.customDistinctUntilChanged( equals: (T?,T?)->Boolean): LiveData<T> {
    val outputLiveData: MediatorLiveData<T> =
        MediatorLiveData()
    outputLiveData.addSource(this, object : Observer<T> {
        var mFirstTime = true
        override fun onChanged(currentValue: T) {
            val previousValue: T? = outputLiveData.value
            if (mFirstTime
                || previousValue == null && currentValue != null
                || previousValue != null && !equals(previousValue , currentValue)
            ) {
                mFirstTime = false
                outputLiveData.value = currentValue
            }
        }
    })
    return outputLiveData
}


fun<T> MutableLiveData<T>.changed(){
    value = value
}


@Suppress("UNCHECKED_CAST")
class MirroredLiveData<T1, T2>(private val mirror: MutableLiveData<T1>) : MediatorLiveData<T2>() {
    private var skipLoopback = false
    override fun setValue(value: T2) {
        super.setValue(value)
        if(!skipLoopback) {
            skipLoopback = true
            mirror.value = value as? T1
            skipLoopback = false
        }
    }
    init {
        addSource(mirror) { currentValue ->
            if(!skipLoopback) {
                skipLoopback = true
                (currentValue as? T2)?.let { setValue(it) }
                skipLoopback = false
            }
        }
    }
}

fun<T1,T2> MutableLiveData<T1>.mirror() : MutableLiveData<T2>{
    return MirroredLiveData<T1,T2>(this)
}


open class LiveDataChildMirror<LD,T>(private val liveData: MutableLiveData<LD>, private val prop: KMutableProperty1<LD,T?>) {
    open operator fun getValue(thisRef: Any?, property: KProperty<*>): T? {
        return liveData.value?.let{prop.get(it)}
    }
    open operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T?) {
        liveData.value?.let{
            prop.set(it, value)
            liveData.changed()
        }
    }
}
fun<LD,T> childMirror(liveData: MutableLiveData<LD>, prop: KMutableProperty1<LD,T?>) = LiveDataChildMirror(liveData,prop)



@OptIn(ExperimentalTypeInference::class)
fun <I, O> Flow<I>.toLiveData(context: CoroutineContext = EmptyCoroutineContext, @BuilderInference  collector: suspend MutableLiveData<O>.(I) -> Unit): MutableLiveData<O> {
    return object : MutableLiveData<O>() {
        var job: Deferred<Unit>? = null
        val self = this
        val supervisorJob = SupervisorJob(context[Job])
        val scope = CoroutineScope(Dispatchers.Main.immediate + context + supervisorJob)

        override fun onActive() {
            value?.let { return }

            scope.launch {
                collect {
                    job?.cancel()
                    job = async(Dispatchers.Main) {
                        collector(self, it)
                    }
                }
            }
        }

        override fun onInactive() {
            super.onInactive()
            job?.cancel()
        }
    }
}