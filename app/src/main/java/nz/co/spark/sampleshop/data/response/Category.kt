package nz.co.spark.sampleshop.data.response
import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import kotlinx.android.parcel.Parcelize
import com.google.gson.annotations.SerializedName as SN

enum class SortType(val displayName: String, val sort: String, val order: String){
    Default       ("Default"         ,        "sort_order",      "ASC"),
    NameAsc       ("Name (A - Z)"    ,        "name"      ,      "ASC"),
    NameDesc      ("Name (Z - A)"    ,        "name"      ,      "DESC"),
    PriceAsc      ("Price (Lowest)"  ,        "price"     ,      "ASC"),
    PriceDesc     ("Price (Highest)" ,        "price"     ,      "DESC"),
    RatingAsc     ("Rating (Highest)",        "rating"    ,      "DESC"),
    RatingDes     ("Rating (Lowest)" ,        "rating"    ,      "ASC")
}


@Parcelize
data class Category(
    @SN("category_id")
    var id: String,
    @SN("parent_id")
    var parentId: String,
    @SN("name")
    var name: String,
    @SN("image")
    var image: String
) : Parcelable{
    val normalizedName get() = name.replace("&amp;", "&")
}

class CategoryDiffUtilCallback : DiffUtil.ItemCallback<Category>() {
    override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean {
        return oldItem.name == newItem.name
                && oldItem.image == newItem.image
    }

}