package nz.co.spark.sampleshop.bottomsheet

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.view.ContextThemeWrapper
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentManager
import nz.co.spark.sampleshop.R
import nz.co.spark.sampleshop.extensions.setView
import nz.co.spark.sampleshop.extensions.setVm
import nz.co.spark.sampleshop.viewmodel.BaseViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel


abstract class BaseBottomSheet<B : ViewDataBinding, VM: BaseViewModel> : BottomSheetDialogFragment(),
    CoroutineScope by CoroutineScope(Dispatchers.Main) {

    @LayoutRes
    abstract fun layout(): Int
    protected abstract val viewModel: VM

    protected lateinit var binding: B

    open fun getMyTag(): String? {
        return javaClass.toString()
    }

    override fun onActivityCreated(arg0: Bundle?) {
        super.onActivityCreated(arg0)
        //set animation for all bottomSheets
        dialog?.let {
            it.window?.attributes?.windowAnimations = R.style.DialogAnimation
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.RoundBottomSheet)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val ans = super.onCreateDialog(savedInstanceState)
        ans.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)

        return ans
    }

    @CallSuper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val contextThemeWrapper = ContextThemeWrapper(activity, R.style.AppTheme)


        val localInflater = inflater.cloneInContext(contextThemeWrapper)

        binding = DataBindingUtil.inflate(localInflater, layout(), container, false)
        binding.lifecycleOwner = this

        binding.setView(this)
        binding.setVm(viewModel)
        onBindingCreated(savedInstanceState)
        registerObservers()

        return binding.root
    }

    open fun onBindingCreated(savedInstanceState: Bundle?) {}
    open fun registerObservers() {}


    override fun onDestroy() {
        super.onDestroy()
        cancel()
    }


    open fun show(manager: FragmentManager) {
        show(manager, getMyTag())
    }

    override fun show(
            manager: FragmentManager,
            tag: String?
    ) {
        try {
            val ft = manager.beginTransaction()
            ft.add(this, tag)
            ft.setCustomAnimations(R.anim.fragment_animation_enter_alpha, R.anim.fragment_animation_exit_alpha)
            ft.commitAllowingStateLoss()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }



}

