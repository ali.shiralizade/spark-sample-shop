package nz.co.spark.sampleshop.util

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.View
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.daimajia.easing.Glider
import com.daimajia.easing.Skill

object AnimationUtils {
    fun glide(skill: Skill?, view: View?, propertyName: String?, vararg values: Float): ValueAnimator {
        return glide(skill, 1f, 0, view, propertyName, *values)
    }

    fun glide(skill: Skill?, duration: Float, view: View?, propertyName: String?, vararg values: Float): ValueAnimator {
        return glide(skill, duration, 0, view, propertyName, *values)
    }

    fun glide(skill: Skill?, duration: Float, startDelay: Long, view: View?, propertyName: String?, vararg values: Float): ValueAnimator {
        val ans = Glider.glide(skill, duration, ObjectAnimator.ofFloat(view, propertyName, *values))
        //        ans.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationStart(Animator animation, boolean isReverse) {
//                view.setVisibility(View.VISIBLE);
//            }
//        });
        ans.startDelay = startDelay
        return ans
    }

    fun transposeNormal(value: Float, to_min: Float, to_max: Float): Float {
        return transpose(value, 0f, 1f, to_min, to_max)
    }

    fun transpose(value: Float, from_min: Float, from_max: Float, to_min: Float, to_max: Float): Float { //0-1 -> 2-5
        val scale = (to_max - to_min) / (from_max - from_min)
        return (value - from_min) * scale + to_min
    }

    fun roundedRect(context: Context, @ColorRes color: Int, radius: Int): GradientDrawable {
        val shape = GradientDrawable()
        shape.shape = GradientDrawable.RECTANGLE
        shape.cornerRadii = floatArrayOf(radius.toFloat(), radius.toFloat(), radius.toFloat(), radius.toFloat(), 0f, 0f, 0f, 0f)
        shape.setColor(ContextCompat.getColor(context, color))
        return shape
    }
}