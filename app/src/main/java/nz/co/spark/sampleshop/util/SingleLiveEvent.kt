package nz.co.spark.sampleshop.util


import android.os.Looper
import android.util.Log
import androidx.annotation.MainThread
import androidx.collection.ArraySet
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer


open class SingleLiveEvent<T> : MediatorLiveData<T>() {

    private val observers = ArraySet<ObserverWrapper<in T>>()

    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        val wrapper = ObserverWrapper(observer)
        observers.add(wrapper)
        super.observe(owner, wrapper)
    }

    @MainThread
    override fun observeForever(observer: Observer<in T>) {
        val wrapper = ObserverWrapper(observer)
        observers.add(wrapper)
        super.observeForever(wrapper)
    }

    @MainThread
    override fun removeObserver(observer: Observer<in T>) {
//        Log.w("removeObserver", "remove called , size: ${observers.size}")
        if (observers.remove(observer)) {
            super.removeObserver(observer)
//            Log.w("removeObserver", "removed normal called , size: ${observers.size}")
            return
        }
        val iterator = observers.iterator()
        while (iterator.hasNext()) {
            val wrapper = iterator.next()
            if (wrapper.observer == observer) {
                iterator.remove()
                super.removeObserver(wrapper)
//                Log.w("removeObserver", "operation normal called , size: ${observers.size}")
                break
            }
        }
        super.removeObserver(observer)
//        Log.w("SingleLiveEvent", "Abnormal state reached , size: ${observers.size}")
    }

    @MainThread
    override fun setValue(t: T?) {
        observers.forEach { it.newValue() }
        super.setValue(t)
    }

    private class ObserverWrapper<T>(val observer: Observer<T>) : Observer<T> {

        private var pending = false

        override fun onChanged(t: T?) {
            if (pending) {
                pending = false
                observer.onChanged(t)
            }
        }

        fun newValue() {
            pending = true
        }
    }

    fun emit(t: T? = null){
        if (Looper.myLooper() == Looper.getMainLooper()) {
            value = t
        } else {
            postValue(t)
        }
    }

}

//open class SingleLiveEventStandard<T> : MediatorLiveData<T>() {
//
//
//    private val mPending = AtomicBoolean(false)
//
//    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
//        if (hasActiveObservers()) {
//            Log.w("SingleLiveEvent", "Multiple observers registered but only one will be notified of changes.")
//        }
//
//        // Observe the internal MutableLiveData
//        super.observe(owner, Observer { t ->
//            if (mPending.compareAndSet(true, false)) {
//                observer.onChanged(t)
//            }
//        })
//    }
//
//    @MainThread
//    override fun setValue(@Nullable t: T?) {
//        mPending.set(true)
//        super.setValue(t)
//    }
//
//    fun emit(t: T? = null){
//        if (ArchTaskExecutor.getInstance().isMainThread) {
//            value = t
//        } else {
//            postValue(t)
//        }
//    }
//
//}
