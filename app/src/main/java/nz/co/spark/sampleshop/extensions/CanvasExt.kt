package nz.co.spark.sampleshop.extensions

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PointF
import kotlin.math.tan


class Parallelogram {
    val points = Array(4){PointF()}

    fun fill(topLeftX: Float, topLeftY: Float, width: Float, height: Float, angle: Float){
        points[0].x = topLeftX
        points[0].y = topLeftY

        points[1].x = points[0].x + width
        points[1].y = points[0].y

        points[2].x = points[1].x + height/tan(angle)
        points[2].y = points[1].y + height

        points[3].x = points[2].x - width
        points[3].y = points[2].y
    }

    fun moveBy(X: Float, Y: Float){
        points.forEach { it.x += X; it.y += Y }
    }
    fun moveX(X: Float){
        points.forEach { it.x += X }
    }
    fun moveY(Y: Float){
        points.forEach { it.y += Y }
    }

    val topLeftX get() = points[0].x
    val topLeftY get() = points[0].y
}

private val drawPolygonPath = Path()
fun Canvas.drawParallelogram(parallelogram: Parallelogram, paint: Paint) = drawPolygon(parallelogram.points, paint)
fun Canvas.drawPolygon(polygon: Array<PointF>, paint: Paint){
    if (polygon.size < 2) {
        return
    }

    // path
    drawPolygonPath.reset()
    drawPolygonPath.moveTo(polygon[0].x, polygon[0].y)

    for (point in polygon)
    {
        drawPolygonPath.lineTo(point.x, point.y)
    }
    drawPolygonPath.lineTo(polygon[0].x, polygon[0].y)

    // draw
    drawPath(drawPolygonPath, paint)
}
