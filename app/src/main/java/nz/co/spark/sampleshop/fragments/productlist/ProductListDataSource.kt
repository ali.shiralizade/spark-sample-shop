package nz.co.spark.sampleshop.fragments.productlist

import android.app.SearchManager.QUERY
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import nz.co.spark.sampleshop.api.apiCall
import nz.co.spark.sampleshop.data.model.Status
import nz.co.spark.sampleshop.data.response.Product


class ProductListDataSource(
    private val scope: CoroutineScope,
    var category: String? = null,
    var sort: String = "p.sort",
    var order: String = "ASC",
    private val networkStateCallback : (Status)->Unit = {}
) : PageKeyedDataSource<Int, Product>() {

    private var job: Job? = null

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Product>) {
        val category = category ?: return
        job = scope.launch {
            try {
                networkStateCallback(Status.LOADING)
                val response = apiCall { getProductList(category, params.requestedLoadSize, 1, sort, order ) }
                if (response.isSuccess()) {
                    callback.onResult(response.data ?: listOf(), 0, 2)
                    networkStateCallback(Status.SUCCESS)
                } else {
                    networkStateCallback(Status.ERROR)
                }
            }catch (exception : Exception){
                Log.e("ProductListDataSource", "Failed to fetch data!")
                networkStateCallback(Status.ERROR)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Product>) {
        val category = category ?: return
        job = scope.launch {
            try {
                networkStateCallback(Status.LOADING)
                val response = apiCall { getProductList(category, params.requestedLoadSize, params.key, sort, order ) }
                if (response.isSuccess()) {
                    callback.onResult(response.data ?: listOf(), params.key + 1)
                    networkStateCallback(Status.SUCCESS)
                } else {
                    networkStateCallback(Status.ERROR)
                }
            }catch (exception : Exception){
                Log.e("ProductListDataSource", "Failed to fetch data!")
                networkStateCallback(Status.ERROR)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Product>) {
        val category = category ?: return
        job = scope.launch {
            try {
                //server returns answer for 0 or negative values
                if(params.key > 0){
                    val response = apiCall { getProductList(category, params.requestedLoadSize, params.key, sort, order ) }
                    if (response.isSuccess()) {
                        callback.onResult(response.data ?: listOf(), params.key - 1)
                        networkStateCallback(Status.SUCCESS)
                    } else {
                        networkStateCallback(Status.ERROR)
                    }
                } else {
                    networkStateCallback(Status.SUCCESS)
                }
            }catch (exception : Exception){
                Log.e("ProductListDataSource", "Failed to fetch data!")
                networkStateCallback(Status.ERROR)
            }
        }
    }

    override fun invalidate() {
        job?.cancel()
        super.invalidate()
    }

}