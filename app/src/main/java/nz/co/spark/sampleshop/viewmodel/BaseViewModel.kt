package nz.co.spark.sampleshop.viewmodel

import androidx.lifecycle.ViewModel
import nz.co.spark.sampleshop.api.AppApi
import nz.co.spark.sampleshop.di.inject
import nz.co.spark.sampleshop.util.AppPreferences

abstract class BaseViewModel : ViewModel() {
    val prefs : AppPreferences by inject()
    val api : AppApi by inject()
}
