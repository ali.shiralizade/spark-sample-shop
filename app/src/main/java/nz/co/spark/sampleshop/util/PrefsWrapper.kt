package nz.co.spark.sampleshop.util

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import nz.co.spark.sampleshop.extensions.fromJson
import nz.co.spark.sampleshop.extensions.json
import nz.co.spark.sampleshop.extensions.setOrPostValue
import kotlin.reflect.KProperty

@Suppress("UNCHECKED_CAST", "unused")
abstract class PrefsWrapper(private var context: Context) {

    //region Listeners
    interface SharedPrefsListener {
        fun onSharedPrefChanged(property: KProperty<*>)
    }
    fun addListener(sharedPrefsListener: SharedPrefsListener) = listeners.add(sharedPrefsListener)
    fun removeListener(sharedPrefsListener: SharedPrefsListener) = listeners.remove(sharedPrefsListener)
    fun clearListeners() = listeners.clear()

    private fun onPrefChanged(property: KProperty<*>) {
        listeners.forEach { it.onSharedPrefChanged(property) }
    }
    private val listeners = mutableListOf<SharedPrefsListener>()
    //endregion

    val prefs: SharedPreferences by lazy {
        context.getSharedPreferences("${context.packageName}.${javaClass.simpleName}", Context.MODE_PRIVATE)
    }

    abstract inner class NonNullPrefDelegate<T>(var prefKey: String?) {
        operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
            if(prefKey == null){ prefKey = property.name }
            return getValue(prefKey ?: property.name)
        }
        operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
            if(prefKey == null){ prefKey = property.name }
            setValue(prefKey ?: property.name, value)
            onPrefChanged(property)
        }
        abstract fun getValue(preferencesKey: String): T
        abstract fun setValue(preferencesKey: String, value: T)
    }

    abstract inner class NullablePrefDelegate<T>(var prefKey: String?) {
        operator fun getValue(thisRef: Any?, property: KProperty<*>): T? {
            if(prefKey == null){ prefKey = property.name }
            return getValue(prefKey ?: property.name)
        }
        operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T?) {
            if(prefKey == null){ prefKey = property.name }
            setValue(prefKey ?: property.name, value)
            onPrefChanged(property)
        }
        abstract fun getValue(preferencesKey: String): T?
        abstract fun setValue(preferencesKey: String, value: T?)
    }



    fun intPref(defaultValue: Int = 0, prefKey: String? = null) : NonNullPrefDelegate<Int> = object : NonNullPrefDelegate<Int>(prefKey) {
        override fun getValue(preferencesKey: String): Int =
            prefs.getInt(preferencesKey, defaultValue)
        override fun setValue(preferencesKey: String, value: Int) =
            prefs.edit().putInt(preferencesKey, value).apply()
    }

    fun floatPref(defaultValue: Float = 0f, prefKey: String? = null) : NonNullPrefDelegate<Float> = object : NonNullPrefDelegate<Float>(prefKey) {
        override fun getValue(preferencesKey: String): Float =
            prefs.getFloat(preferencesKey, defaultValue)
        override fun setValue(preferencesKey: String, value: Float) =
            prefs.edit().putFloat(preferencesKey, value).apply()
    }

    fun booleanPref(defaultValue: Boolean = false, prefKey: String? = null) : NonNullPrefDelegate<Boolean> = object : NonNullPrefDelegate<Boolean>(prefKey) {
        override fun getValue(preferencesKey: String): Boolean =
            prefs.getBoolean(preferencesKey, defaultValue)
        override fun setValue(preferencesKey: String, value: Boolean) =
            prefs.edit().putBoolean(preferencesKey, value).apply()
    }
    fun longPref(defaultValue: Long = 0L, prefKey: String? = null) : NonNullPrefDelegate<Long> = object : NonNullPrefDelegate<Long>(prefKey) {
        override fun getValue(preferencesKey: String): Long =
            prefs.getLong(preferencesKey, defaultValue)
        override fun setValue(preferencesKey: String, value: Long) =
            prefs.edit().putLong(preferencesKey, value).apply()
    }


    fun stringPref(defaultValue: String? = null, prefKey: String? = null) : NullablePrefDelegate<String> = object : NullablePrefDelegate<String>(prefKey) {
        override fun getValue(preferencesKey: String): String? =
            prefs.getString(preferencesKey, defaultValue)
        override fun setValue(preferencesKey: String, value: String?) =
            prefs.edit().putString(preferencesKey, value).apply()
    }
    fun stringSetPref(defaultValue: Set<String>? = HashSet(), prefKey: String? = null) : NullablePrefDelegate<Set<String>> = object : NullablePrefDelegate<Set<String>>(prefKey) {
        override fun getValue(preferencesKey: String): Set<String>? =
            prefs.getStringSet(preferencesKey, defaultValue)
        override fun setValue(preferencesKey: String, value: Set<String>?) =
            prefs.edit().putStringSet(preferencesKey, value).apply()
    }
    inline fun <reified T : Any?> pref(defaultValue: T? = null, prefKey: String? = null) : NullablePrefDelegate<T> = object : NullablePrefDelegate<T>(prefKey) {
        override fun getValue(preferencesKey: String): T? =
            prefs.getString(preferencesKey, "")?.fromJson<T>(T::class.java) ?: defaultValue
        override fun setValue(preferencesKey: String, value: T?) =
            prefs.edit().putString(preferencesKey, (value as? Any)?.json ?: "").apply()
    }





    inner class NullableLiveDataPrefDelegate<T : Any?> (var innerValue: NullablePrefDelegate<T>)  {
        private var isInitialized = false
        private val liveData : MutableLiveData<T?> = object : MutableLiveData<T?>() {
            override fun setValue(value: T?) {
                if(isInitialized) {
                    innerValue.prefKey?.let { prefKey ->
                        innerValue.setValue(prefKey, value)
                    }
                }

                super.setValue(value)
            }
        }

        operator fun getValue(thisRef: Any?, property: KProperty<*>): MutableLiveData<T?> {
            if(innerValue.prefKey == null){
                innerValue.prefKey = property.name
            }
            if(!isInitialized) {
                innerValue.getValue(innerValue.prefKey ?: property.name)?.let { prevVal ->
                    liveData.setOrPostValue(prevVal)
                }
                isInitialized = true
            }
            return liveData
        }

    }
    inner class NonNullLiveDataPrefDelegate<T : Any> (var innerValue: NonNullPrefDelegate<T>)  {
        private var isInitialized = false
        private val liveData : MutableLiveData<T> = object : MutableLiveData<T>() {
            override fun setValue(value: T) {
                if(isInitialized) {
                    innerValue.prefKey?.let { prefKey ->
                        innerValue.setValue(prefKey, value)
                    }
                }

                super.setValue(value)
            }
        }

        operator fun getValue(thisRef: Any?, property: KProperty<*>): MutableLiveData<T> {
            if(innerValue.prefKey == null){
                innerValue.prefKey = property.name
            }
            if(!isInitialized) {
                liveData.setOrPostValue(innerValue.getValue(innerValue.prefKey ?: property.name))
                isInitialized = true
            }
            return liveData
        }
    }

    inline fun <reified T : Any> liveData(nonNullInnerPrefs: NonNullPrefDelegate<T>) =
        NonNullLiveDataPrefDelegate(nonNullInnerPrefs)
    inline fun <reified T : Any?> liveData(nullablePrefs: NullablePrefDelegate<T?>) =
        NullableLiveDataPrefDelegate(nullablePrefs)




}