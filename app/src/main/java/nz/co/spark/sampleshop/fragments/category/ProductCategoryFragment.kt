package nz.co.spark.sampleshop.fragments.category

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import nz.co.spark.sampleshop.R
import nz.co.spark.sampleshop.adapter.CategoryAdapter
import nz.co.spark.sampleshop.data.response.Category
import nz.co.spark.sampleshop.databinding.FragmentProductCategoryBinding
import nz.co.spark.sampleshop.databinding.FragmentSplashBinding
import nz.co.spark.sampleshop.fragments.BaseFragment
import nz.co.spark.sampleshop.fragments.observe
import nz.co.spark.sampleshop.fragments.productlist.ProductListFragmentArgs
import nz.co.spark.sampleshop.util.NavAnimations


class ProductCategoryFragment :
    BaseFragment<FragmentProductCategoryBinding, ProductCategoryFragmentVM>() {
    override val viewModel: ProductCategoryFragmentVM by viewModels()
    override fun layout() = R.layout.fragment_product_category

    private val categoryAdapter = CategoryAdapter(ArrayList()){ _, category ->
        if(category != null)
            navigate(ProductCategoryFragmentDirections.navigateCategoryToList(category) )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.apply {
            adapter = categoryAdapter
        }

        viewModel.getProductCategories()

    }

    override fun registerObservers() {

        observe(viewModel.categoriesResponse) {
            if (it.isSuccess()) run {
                it.data?.let { list ->
                    categoryAdapter.setItem(list)
                }

            }
        }
    }


}