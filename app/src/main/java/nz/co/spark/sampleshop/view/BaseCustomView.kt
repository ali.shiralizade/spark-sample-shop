package nz.co.spark.sampleshop.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding


abstract class BaseCustomView<B : ViewDataBinding> : RelativeLayout {

    @LayoutRes abstract fun layout(): Int

    protected lateinit var binding: B


    constructor(context: Context?) : super(context){
        this.init(null)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){
        this.init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        this.init(attrs)
    }


    @CallSuper
    protected open fun init(attrs: AttributeSet?) {

        if (isInEditMode) {
            LayoutInflater.from(context).inflate(layout(), this, true)
        }
        else{
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), layout(), this, true)
        }
    }

    protected open fun onGlobalLayout() {
    }





}