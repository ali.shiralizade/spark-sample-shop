package nz.co.spark.sampleshop.repository

import nz.co.spark.sampleshop.api.apiCall
import nz.co.spark.sampleshop.data.model.Resource
import nz.co.spark.sampleshop.data.response.Category

object CategoryRepository {
    suspend fun getProductCategories(): Resource<ArrayList<Category>> {
        return apiCall { getProductCategories() }
    }
}