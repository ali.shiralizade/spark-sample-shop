package nz.co.spark.sampleshop.repository

import nz.co.spark.sampleshop.api.apiCall
import nz.co.spark.sampleshop.data.model.Resource
import nz.co.spark.sampleshop.data.response.SessionResponse
import nz.co.spark.sampleshop.di.inject
import nz.co.spark.sampleshop.util.AppPreferences

object UserRepository {

    private val prefs : AppPreferences by inject()

    suspend fun requestSession(): Resource<SessionResponse> {
        val ans : Resource<SessionResponse>
        val lastSession = prefs.session
        if(lastSession.isNullOrEmpty()){
            ans = apiCall { requestSession() }
            if (ans.isSuccess()){
                val session = ans.data?.session
                if (!session.isNullOrEmpty()){
                    prefs.session = session
                }
            }
        }
        else {
            ans = Resource.success(SessionResponse(lastSession))
        }
        return ans
    }


}