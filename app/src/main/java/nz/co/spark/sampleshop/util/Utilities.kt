package nz.co.spark.sampleshop.util

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.Settings
import android.view.View
import androidx.core.app.ShareCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentActivity
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


object Utilities {


    fun openInBrowser(context: Context, url: String) {
        if (url.startsWith("http://") || url.startsWith("https://")) {
            val uris = Uri.parse(url)
            val intents = Intent(Intent.ACTION_VIEW, uris)
            context.startActivity(intents)
        }
    }

    fun openInternetSettings(context: Context): Boolean {
        val settingsPackage = "com.android.settings"
        val dataUsageClass = "com.android.settings.Settings\$DataUsageSummaryActivity"
        try {
            val intent = Intent(Intent.ACTION_MAIN, null)
            val cn = ComponentName(settingsPackage, dataUsageClass)
            intent.component = cn
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
            return true
        } catch (e: Exception) {
            try {
                val intent = Intent(Settings.ACTION_WIRELESS_SETTINGS)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(intent)
            } catch (ignored: Exception) {
                try {
                    val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    context.startActivity(intent)
                } catch (ignored2: Exception) {
                }
            }
        }
        return false
    }


    fun shareContent(activity:FragmentActivity,subject: String,shareUsing:String,content:String){
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        sharingIntent.putExtra(Intent.EXTRA_TEXT, content)
        activity.startActivity(Intent.createChooser(sharingIntent,shareUsing))
    }




}

fun View.bitmap() : Bitmap?{
    val canvasBitmap = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(canvasBitmap)
    val bgDrawable: Drawable? = background
    if (bgDrawable != null) bgDrawable.draw(canvas) else canvas.drawColor(Color.WHITE)
    canvas.save()
    draw(canvas)
    canvas.restore()
    return canvasBitmap
}

fun Context.screenshotFile(filename: String):File {
    val cacheFile = File(cacheDir, "screenshots/$filename")
    cacheFile.parentFile?.mkdirs()
    return cacheFile
}

fun Bitmap.saveScreenshot(context: Context, filename: String){
    try {
        FileOutputStream(context.screenshotFile(filename)).use { out ->
            compress(Bitmap.CompressFormat.PNG, 100, out)
        }
    } catch (e: IOException) {
        e.printStackTrace()
    }
}

fun Activity.shareScreenshot(filename: String, title: String, subject: String){
    val uri: Uri = FileProvider.getUriForFile(this, "${packageName}.cache.fileprovider", screenshotFile(filename))

    val intent: Intent = ShareCompat.IntentBuilder.from(this)
            .setType("image/jpg")
            .setSubject(subject)
            .setStream(uri)
            .setChooserTitle(title)
            .createChooserIntent()
            .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

    startActivity(intent)
}




