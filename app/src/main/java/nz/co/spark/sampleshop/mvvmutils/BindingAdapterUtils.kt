@file:Suppress("unused")

package nz.co.spark.sampleshop.mvvmutils

import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.RelativeSizeSpan
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.ViewCompat
import androidx.databinding.BindingAdapter
import nz.co.spark.sampleshop.R
import nz.co.spark.sampleshop.util.CircleTransformation
import nz.co.spark.sampleshop.util.PixelUtil
import nz.co.spark.sampleshop.util.RoundedCornersTransformation
import nz.co.spark.sampleshop.util.StringUtility
import nz.co.spark.sampleshop.view.MoneyEditText
import com.kyleduo.switchbutton.SwitchButton
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlin.math.abs
import kotlin.math.round


object BindingAdapterUtils {
    @JvmStatic
    @BindingAdapter(value = ["loadCircleFromUrl", "placeholder","onErrorCallback"], requireAll = false)
    fun setCircleImageUrl(imageView: ImageView, url: String?, drawable: Drawable,onError:((message:String?)->Unit)?=null) {
        if (!url.isNullOrEmpty()) {
            Picasso.get()
                    .load(url)
                    .transform(CircleTransformation())
                    .placeholder(drawable)
                    .into(imageView,object :Callback{
                        override fun onSuccess() {}
                        override fun onError(e: Exception?) {
                            onError?.invoke(e?.message)
                        }
                    })
        } else {
            imageView.setImageDrawable(drawable)
        }
    }


    @JvmStatic
    @BindingAdapter(value = ["loadFromUrl", "placeholder", "radius", "margin"], requireAll = false)
    fun setImageUrl(imageView: ImageView, url: String?, drawable: Drawable?, radius: Int, margin: Int = 0) {
        if (!url.isNullOrEmpty()) {

            val mRadius = PixelUtil.dpToPx(radius.toFloat())

            val loader = Picasso.get().load(url)

            if (mRadius > 0)
                loader.transform(RoundedCornersTransformation(mRadius, margin))
            if (drawable != null)
                loader.placeholder(drawable)

            val w = imageView.measuredWidth
            val h = imageView.measuredHeight
            if( w > 0 && h > 0){
                when(imageView.scaleType){
                    ImageView.ScaleType.MATRIX -> {}
                    ImageView.ScaleType.FIT_XY -> {}
                    ImageView.ScaleType.FIT_START -> {}
                    ImageView.ScaleType.FIT_CENTER -> {}
                    ImageView.ScaleType.FIT_END -> {}
                    ImageView.ScaleType.CENTER -> {}
                    ImageView.ScaleType.CENTER_CROP -> loader.centerCrop().resize(w,h)
                    ImageView.ScaleType.CENTER_INSIDE -> loader.centerInside().resize(w,h)
                    else -> {}
                }
            }

            loader.into(imageView)
        } else {
            imageView.setImageDrawable(drawable)
        }
    }


    @JvmStatic
    @BindingAdapter("android:background")
    fun setBackground(view: View, @DrawableRes resId: Int) {
        view.setBackgroundResource(resId)
    }

    @JvmStatic
    @BindingAdapter("srcCompat")
    fun setImageDrawable(imageView: ImageView, drawable: Int) {
        imageView.setImageDrawable(AppCompatResources.getDrawable(imageView.context, drawable))
    }

    @JvmStatic
    @BindingAdapter("srcCompat")
    fun setImageDrawable(imageView: ImageView, drawable: Drawable) {
        imageView.setImageDrawable(drawable)
    }

    @JvmStatic
    @BindingAdapter("htmlText")
    @Suppress("DEPRECATION")
    fun setHtmlText(view: TextView, html: String?) {
        if (html != null && !TextUtils.isEmpty(html)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                view.text = Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
            } else {
                view.text = Html.fromHtml(html)
            }
        }
    }

    @JvmStatic
    @BindingAdapter("formatAmount")
    fun setFormattedAmount(textView: TextView, amount: Long) {
        textView.text = StringUtility.formatLong(amount)
    }

    @JvmStatic
    @BindingAdapter("formatAmount")
    fun setFormattedAmount(textView: TextView, amount: Int) {
        textView.text = StringUtility.formatInteger(amount)
    }

    @JvmStatic
    @BindingAdapter("formatAmountAbs")
    fun setFormattedAmountAbs(textView: TextView, amount: Long) {
        textView.text = StringUtility.formatLong(abs(amount))
    }

    @JvmStatic
    @BindingAdapter("formatAmountAbs")
    fun setFormattedAmountAbs(textView: TextView, amount: Int) {
        textView.text = StringUtility.formatInteger(abs(amount))
    }

    @JvmStatic
    @BindingAdapter("formatAmountWithCurrency")
    fun setFormattedAmountWithCurrency(textView: TextView, amount: Long) {
        val ctx = textView.context
        val value = ctx.getString(R.string.x_rials, StringUtility.formatLong(amount))
        val span: Spannable = SpannableString(value)
        span.setSpan(
                RelativeSizeSpan(0.8f),
                value.length - 5,
                value.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        textView.text = span
    }

    @JvmStatic
    @BindingAdapter("formatAmountWithCurrency")
    fun setFormattedAmountWithCurrency(textView: TextView, amount: Int) {
        setFormattedAmountWithCurrency(textView, amount.toLong())
    }


    @JvmStatic
    @BindingAdapter("resText")
    fun setText(textView: TextView, resId: Int) {
        textView.text = textView.context.getString(resId)
    }


    @JvmStatic
    @BindingAdapter("sbChecked")
    fun setChecked(switchButton: SwitchButton, checked: Boolean) {
        switchButton.isChecked = checked
    }



    @JvmStatic
    @BindingAdapter("android:layout_marginBottom")
    fun setBottomMargin(view: View, bottomMargin: Float) {
        val layoutParams: ViewGroup.MarginLayoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
        layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.rightMargin, round(bottomMargin).toInt())
        view.layoutParams = layoutParams
    }


    @JvmStatic
    @BindingAdapter("android:layout_marginTop")
    fun setTopMargin(view: View, topMargin: Float) {
        val layoutParams: ViewGroup.MarginLayoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
        layoutParams.setMargins(layoutParams.leftMargin, round(topMargin).toInt(), layoutParams.rightMargin, layoutParams.bottomMargin)
        view.layoutParams = layoutParams
    }

    @JvmStatic
    @BindingAdapter("backgroundTint")
    fun setAppBackgroundTint(view: View?, color: Int) {
        val colorStateList = ColorStateList(arrayOf(intArrayOf(android.R.attr.state_enabled)), intArrayOf(color))
//        if (view is AppCompatTextView) {
//            view.supportBackgroundTintList = colorStateList
//        } else if (view is AppCompatButton) {
//            view.supportBackgroundTintList = colorStateList
//        }
        ViewCompat.setBackgroundTintList(view!!, colorStateList)
    }


    @JvmStatic
    @BindingAdapter("drawableTint")
    fun setDrawableTint(textView: TextView, color: Int) {
        textView.compoundDrawables.filterNotNull().forEach {
            it.colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN)
        }
    }


    @JvmStatic
    @BindingAdapter("amount")
    fun setAmount(moneyAmountEditText: MoneyEditText, amount: Long) {
        moneyAmountEditText.amount = amount
    }

    @JvmStatic
    @BindingAdapter("visibility")
    fun setVisibility(v: View, visibility: Boolean) {
        v.visibility = if (visibility) View.VISIBLE else View.GONE
    }


    @JvmStatic
    @BindingAdapter("parentTranslationX")
    fun setParentTranslationX(v: View, translation: Float) {
        v.clearAnimation()
        val sw = PixelUtil.widthPx
        val pw = ((v.parent as? View)?.width ?: sw)
        if(abs(pw) <= 0.5) {
            v.translationX = sw * translation
        } else {
            v.animate().translationX(pw * translation)
        }
    }

    @JvmStatic
    @BindingAdapter("translationY")
    fun setTranslationY(v: View, translation: Float) {
        val h = v.measuredHeight
        if(abs(h) <= 0.5) {
            v.translationY = 0f
        } else {
            v.animate().translationY(h * translation)
        }
    }



    @JvmStatic
    @BindingAdapter(value = ["android:onClick", "reClickDelay"], requireAll = false)
    fun setOnClick(view: View, onClickListener: View.OnClickListener?, delayIn: Int?) {

        val delay = delayIn ?: 300

        if (delay > 0 && onClickListener != null){
            view.setOnClickListener(object : View.OnClickListener{
                var lastClick:Long = 0
                override fun onClick(view: View?) {
                    val ct = System.currentTimeMillis()
                    if (ct - lastClick > delay) {
                        lastClick = ct
                        onClickListener.onClick(view)
                    }
                }
            })
        } else {
            view.setOnClickListener(onClickListener)
        }
    }
}
