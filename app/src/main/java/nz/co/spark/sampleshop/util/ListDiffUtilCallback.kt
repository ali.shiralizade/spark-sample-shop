package nz.co.spark.sampleshop.util

import androidx.recyclerview.widget.DiffUtil

class ListDiffUtilCallback <T> (private val oldList : List<T>, private val newList : List<T>, val callback: DiffUtil.ItemCallback<T>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        callback.areItemsTheSame(oldList[oldItemPosition], newList[newItemPosition])


    override fun getOldListSize() = oldList.size
    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        callback.areContentsTheSame(oldList[oldItemPosition], newList[newItemPosition])

}