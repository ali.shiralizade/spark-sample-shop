package nz.co.spark.sampleshop.extensions

import com.google.gson.Gson
import nz.co.spark.sampleshop.di.inject
import org.json.JSONObject
import java.lang.reflect.Type

val Any.json: String get() = inject<Gson>().value.toJson(this)

fun <T> String.fromJson(cls: Type): T? {

    try {
        return inject<Gson>().value.fromJson(this, cls)
    }catch (e:Exception){}
    return null
}

inline fun <reified T> String.fromJson(): T? {
    return fromJson(T::class.java)
}

fun String.jsonChild(child: String) = JSONObject(this).getJSONObject(child).toString()
