@file:Suppress("unused")

package nz.co.spark.sampleshop.extensions

import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.regex.Pattern


fun isPhoneNumberValid(str: String?): Boolean {
    if (str == null) return false
    return Pattern.compile("^(?:0098|\\+98|98)?0?(9\\d{9})\$")
        .matcher(str).matches()
}

fun isCityPhoneNumberValid(str: String?): Boolean {
    if (str == null) return false
    return Pattern.compile("^0\\d{2,3}\\d{8}\$").matcher(str).matches()
}

fun String?.isValidPhoneNumber(): Boolean {
    return isPhoneNumberValid(this)
}

fun String?.isValidCityPhoneNumber(): Boolean {
    return isCityPhoneNumberValid(this)
}

fun String.isEmailValid(): Boolean {
    return android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String?.hasAnyPersianChar(): Boolean {
    if (this.isNullOrEmpty())return false
    val pattern = Pattern.compile("[\u0600-\u06FF]+", Pattern.CASE_INSENSITIVE)
    val matcher = pattern.matcher(this)
    return matcher.find()
}

fun String?.normalizeMobile() : String? {
    if (this == null) return null
    val regex = Pattern.compile("^(?:0098|\\+98|98)?0?(9\\d{9})\$").matcher(this)
    if (regex.matches()){
        val ans = regex.group(1)
        if (ans?.length == 10) {
            return "0$ans"
        }
    }
    return null
}

fun String?.normalizeLandline() : String? {
    if (isValidCityPhoneNumber()) {
        return this
    }
    return null
}
fun String?.normalizePhone() : String?{
    return normalizeMobile() ?: normalizeLandline()
}


val String.urlEncoded : String get() {
    return try{
        URLEncoder.encode(this, "utf-8")
    }catch (e: UnsupportedEncodingException){
        this
    }
}