package nz.co.spark.sampleshop.util

import kotlin.random.Random

val random = Random(100)

fun randomFloat(min: Float = 0f, max: Float = 1f) : Float{
    val diff = max-min
    return random.nextFloat() * diff + min
}