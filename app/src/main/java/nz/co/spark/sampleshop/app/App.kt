package nz.co.spark.sampleshop.app

import androidx.multidex.MultiDexApplication
import nz.co.spark.sampleshop.initKoin

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        initKoin(this)

        registerActivityLifecycleCallbacks(AppActivityLifecycleCallback)
    }
}