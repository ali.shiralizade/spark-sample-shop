package nz.co.spark.sampleshop

import android.app.Application
import androidx.annotation.UiThread
import nz.co.spark.sampleshop.di.appModule
import nz.co.spark.sampleshop.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


@Synchronized
@UiThread
fun initKoin(application: Application){

    startKoin {
        // declare used Android context
        androidContext(application)
        // declare modules
        modules(listOf(appModule, networkModule))
    }
}
