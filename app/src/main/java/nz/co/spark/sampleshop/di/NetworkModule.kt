package nz.co.spark.sampleshop.di

import android.util.Log
import com.google.gson.GsonBuilder
import nz.co.spark.sampleshop.BuildConfig
import nz.co.spark.sampleshop.api.AppApi
import nz.co.spark.sampleshop.api.RetrofitInterceptor
import nz.co.spark.sampleshop.util.RemoteServicesUrl
import com.moczul.ok2curl.CurlInterceptor
import com.moczul.ok2curl.logger.Loggable
import nz.co.spark.sampleshop.api.AppRetrofitConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

val networkModule = module {

    single { provideOkHttpClient(RetrofitInterceptor()) }

    single { provideRetrofitInstance( get(), RemoteServicesUrl.BASE_URL) }

    single { get<Retrofit>().create(AppApi::class.java) }

    single { GsonBuilder().create() }
}

fun provideRetrofitInstance(okHttpClient: OkHttpClient,baseUrl:String): Retrofit {
    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(okHttpClient)
        .addConverterFactory(AppRetrofitConverterFactory.create()).build()
}

fun provideOkHttpClient(retrofitInterceptor: RetrofitInterceptor): OkHttpClient {
    val ans = OkHttpClient()
        .newBuilder()
        .addInterceptor(retrofitInterceptor)

    if(BuildConfig.DEBUG){
        ans.addInterceptor(
                HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY)
        )

        ans.connectTimeout(30 , TimeUnit.SECONDS)
        ans.readTimeout(30 , TimeUnit.SECONDS)

        ans.addInterceptor(CurlInterceptor(Loggable { message ->
            Log.i("Ok2Curl", message)
        }))

    }



    return ans.build()
}





