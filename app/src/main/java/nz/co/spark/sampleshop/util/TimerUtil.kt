package nz.co.spark.sampleshop.util

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

fun CoroutineScope.runEvery(delayMs: Long, repeat: Int, action: suspend ()->Unit?){
    launch {
        for (i in 0 until repeat) {
            delay(delayMs)
            action()
        }
    }
}