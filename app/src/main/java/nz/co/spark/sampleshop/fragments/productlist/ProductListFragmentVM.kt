package nz.co.spark.sampleshop.fragments.productlist

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.github.musichin.reactivelivedata.combineLatestWith
import com.github.musichin.reactivelivedata.filter
import nz.co.spark.sampleshop.data.model.Status
import nz.co.spark.sampleshop.data.response.Category
import nz.co.spark.sampleshop.data.response.Product
import nz.co.spark.sampleshop.data.response.SortType
import nz.co.spark.sampleshop.viewmodel.BaseViewModel

class ProductListFragmentVM : BaseViewModel(){

    val productsLiveData : LiveData<PagedList<Product>>


    private lateinit var dataSourceFactory : DataSource.Factory<Int, Product>
    var dataSource : ProductListDataSource? = null

    val networkState = MutableLiveData<Status>(Status.LOADING)

    val category = MutableLiveData<Category>()
    val sort = MutableLiveData<SortType>()

    val categoryAndSortLiveData : MediatorLiveData<Pair<Category, SortType>> = object : MediatorLiveData<Pair<Category, SortType>>(){
        override fun setValue(value: Pair<Category, SortType>?) {
            super.setValue(value)
            reload()
        }
    }.apply {
        addSource(
            category.distinctUntilChanged().combineLatestWith(
                sort.distinctUntilChanged()
            ).filter { (it.first != null && it.second != null) }
        ){
            value = it
        }
    }


    val productsLiveDataLogger = Observer<PagedList<Product>> {
        Log.d("ProductListFragmentVM", "received data")
    }

    override fun onCleared() {
        productsLiveData.removeObserver(productsLiveDataLogger)
    }

    init{

        val config = PagedList.Config.Builder()
            .setPageSize(2)
            .setInitialLoadSizeHint(2) // because there are at most 6 products we set to 2 to get 3 pages :D
            .setEnablePlaceholders(false)
            .build()
        productsLiveData = initializedPagedListBuilder(config).build()

        productsLiveData.observeForever(productsLiveDataLogger)

    }


    fun reload() = categoryAndSortLiveData.value?.let{ (category, sortType) ->

        if(category.id.isEmpty()) return@let

        dataSource?.category = category.id
        dataSource?.sort = sortType.sort
        dataSource?.order = sortType.order

        dataSource?.invalidate()

    }

    private fun initializedPagedListBuilder(config: PagedList.Config) : LivePagedListBuilder<Int, Product> {

        dataSourceFactory = object : DataSource.Factory<Int, Product>() {
            override fun create(): DataSource<Int, Product> {
                val category = categoryAndSortLiveData.value?.first?.id
                val sortType = categoryAndSortLiveData.value?.second ?: SortType.Default
                dataSource = ProductListDataSource(viewModelScope, category, sortType.sort, sortType.order){
                    networkState.postValue(it)
                }
                return dataSource!!
            }
        }
        return LivePagedListBuilder<Int, Product>(dataSourceFactory, config)
    }

}