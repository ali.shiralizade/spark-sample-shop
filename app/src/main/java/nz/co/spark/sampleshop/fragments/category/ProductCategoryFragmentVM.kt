package nz.co.spark.sampleshop.fragments.category

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import nz.co.spark.sampleshop.data.model.Resource
import nz.co.spark.sampleshop.data.response.Category
import nz.co.spark.sampleshop.data.response.SessionResponse
import nz.co.spark.sampleshop.di.inject
import nz.co.spark.sampleshop.repository.CategoryRepository
import nz.co.spark.sampleshop.repository.UserRepository
import nz.co.spark.sampleshop.viewmodel.BaseViewModel

class ProductCategoryFragmentVM : BaseViewModel(){


    val categoriesResponse = MutableLiveData<Resource<ArrayList<Category>>>()

    fun getProductCategories(){
        if(categoriesResponse.value?.data?.isNotEmpty() == true ){
            return
        }
        viewModelScope.launch {
            categoriesResponse.value = Resource.loading()
            categoriesResponse.value = CategoryRepository.getProductCategories()
        }
    }

}