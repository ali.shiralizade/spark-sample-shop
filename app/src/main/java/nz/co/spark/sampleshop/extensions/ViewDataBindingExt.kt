package nz.co.spark.sampleshop.extensions

import androidx.databinding.ViewDataBinding
import nz.co.spark.sampleshop.viewmodel.BaseViewModel

fun ViewDataBinding.setView(view: Any){
    try {
        val setViewMethod = this.javaClass.getMethod("setView", view.javaClass)
        setViewMethod.invoke(this, view)
    } catch (ignored: Throwable) {
    }
}
fun ViewDataBinding.setVm(viewModel: BaseViewModel){
    try {
        val setVmMethod = this.javaClass.getMethod("setVm", viewModel.javaClass)
        setVmMethod.invoke(this, viewModel)
    } catch (ignored: Throwable) {
    }
}