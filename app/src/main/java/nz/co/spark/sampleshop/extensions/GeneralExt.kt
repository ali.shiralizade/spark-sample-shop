package nz.co.spark.sampleshop.extensions

import nz.co.spark.sampleshop.data.model.NetworkError


fun NetworkError?.getCustomErrorMessage(): String {
    val responseCode=this?.responseCode?.let { "($it)" }
    val commonError = " خطایی پیش آمده است ${responseCode ?: ""}"
    if (this == null) return commonError
    return if (this.message?.hasAnyPersianChar() == true) {
        this.message ?: commonError
    } else {
        commonError
    }
}