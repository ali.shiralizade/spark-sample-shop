package nz.co.spark.sampleshop.api

import nz.co.spark.sampleshop.di.inject
import nz.co.spark.sampleshop.util.AppPreferences
import okhttp3.Interceptor
import okhttp3.Response


class RetrofitInterceptor : Interceptor {

    private val prefs: AppPreferences by inject()


    override fun intercept(chain: Interceptor.Chain): Response {

        val request = chain.request().newBuilder().apply {

            addHeader("X-Oc-Merchant-Id", "Androidcart1q@W3e")

            prefs.session?.let { session ->
                if(session.isNotEmpty()){
                    addHeader("X-Oc-Session", session)
                }
            }
        }.build()

        return chain.proceed(request)
    }

}
