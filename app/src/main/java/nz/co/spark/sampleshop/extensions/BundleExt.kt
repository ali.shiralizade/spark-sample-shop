package nz.co.spark.sampleshop.extensions



import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import androidx.fragment.app.Fragment
import java.io.Serializable

fun<T:Fragment> T.args(bundle: Bundle) : T{
    arguments = bundle
    return this
}

@Suppress("UNCHECKED_CAST")
fun Bundle.put(key: String, value: Any): Bundle {
    when (value) {
        is Boolean -> putBoolean(key, value)
        is Byte -> putByte(key, value)
        is Char -> putChar(key, value)
        is Short -> putShort(key, value)
        is Int -> putInt(key, value)
        is Long -> putLong(key, value)
        is Float -> putFloat(key, value)
        is Double -> putDouble(key, value)
        is String -> putString(key, value)
        is CharSequence -> putCharSequence(key, value)

        is Serializable -> putSerializable(key, value)
        is BooleanArray -> putBooleanArray(key, value)
        is ByteArray -> putByteArray(key, value)
        is ShortArray -> putShortArray(key, value)
        is CharArray -> putCharArray(key, value)
        is IntArray -> putIntArray(key, value)
        is LongArray -> putLongArray(key, value)
        is FloatArray -> putFloatArray(key, value)
        is DoubleArray -> putDoubleArray(key, value)

        is Parcelable -> putParcelable(key, value)


        is ArrayList<*> -> {
            var firstNonNullElement: Any? = null
            for (obj in value){
                if(obj != null) {
                    firstNonNullElement = obj
                    break
                }
            }
            if( firstNonNullElement != null){
                when(firstNonNullElement){
                    is Int -> putIntegerArrayList(key, value as ArrayList<Int>)
                    is String -> putStringArrayList(key, value as ArrayList<String>)
                    is CharSequence -> putCharSequenceArrayList(key, value as ArrayList<CharSequence>)
                    is Parcelable -> putParcelableArrayList(key, value as ArrayList<Parcelable>)
                }
            }
        }
        is Array<*> -> {
            var firstNonNullElement: Any? = null
            for (obj in value){
                if(obj != null) {
                    firstNonNullElement = obj
                    break
                }
            }
            if( firstNonNullElement != null){
                when(firstNonNullElement){
                    is String -> putStringArray(key, value as Array<String>)
                    is CharSequence -> putCharSequenceArray(key, value as Array<CharSequence>)
                    is Parcelable -> putParcelableArray(key, value as Array<Parcelable>)
                }
            }
        }



    }

    return this
}


fun<T:Parcelable> T.shallowCopy(creator : Parcelable.Creator<T>) : T {
    val parcel = Parcel.obtain()
    writeToParcel(parcel, 0)
    parcel.setDataPosition(0)
    val ans = creator.createFromParcel(parcel)
    parcel.recycle()
    return ans ?: this
}