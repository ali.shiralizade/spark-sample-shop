package nz.co.spark.sampleshop.extensions

import android.content.Context
import android.content.pm.PackageManager

val Context?.metaData
    get() = try {
        this?.packageManager?.getApplicationInfo(this.packageName, PackageManager.GET_META_DATA)?.metaData
    } catch (e: PackageManager.NameNotFoundException) {
        null
    }