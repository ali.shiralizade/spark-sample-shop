package nz.co.spark.sampleshop.extensions

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import com.squareup.picasso.Target

fun Picasso.cache(vararg urls: String, transformer: (RequestCreator.() -> RequestCreator)? = null) {
    for (url in urls) {

        if (url.isNotEmpty()) {

            var creator = load(url)

            if (transformer != null) {
                creator = transformer(creator)
            }

            creator.into(object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {}
            })
        }
    }
}