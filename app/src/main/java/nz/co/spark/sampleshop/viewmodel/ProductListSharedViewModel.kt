package nz.co.spark.sampleshop.viewmodel

import androidx.lifecycle.MutableLiveData
import nz.co.spark.sampleshop.data.response.SortType

class ProductListSharedViewModel : BaseViewModel() {
    val sort = MutableLiveData(SortType.Default)
}