package nz.co.spark.sampleshop.adapter
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import nz.co.spark.sampleshop.data.response.Category
import nz.co.spark.sampleshop.data.response.CategoryDiffUtilCallback
import nz.co.spark.sampleshop.databinding.ItemCategoryBinding
import nz.co.spark.sampleshop.util.ListDiffUtilCallback

class CategoryAdapter(private var categories: ArrayList<Category>, val onClickListener: (Int, Category?) -> Unit) : RecyclerView.Adapter<CategoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCategoryBinding.inflate(inflater)
        return CategoryViewHolder(binding, onClickListener)
    }

    override fun getItemCount(): Int = categories.size

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) = holder.bind(categories[position])


    fun setItem(items:ArrayList<Category>){
        val diffCallback = ListDiffUtilCallback(categories, items, CategoryDiffUtilCallback())
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.categories = items
        diffResult.dispatchUpdatesTo(this)
    }


}


class CategoryViewHolder(val binding: ItemCategoryBinding, val onClickListener: (Int, Category?) -> Unit) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.view = this
    }
    fun bind(category: Category) {
        binding.category = category
    }

    fun onClick(){
        onClickListener(adapterPosition, binding.category)
    }

}