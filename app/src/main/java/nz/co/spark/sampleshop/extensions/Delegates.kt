package nz.co.spark.sampleshop.extensions

import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty0

open class Mirror<T>(private val mirror: KProperty0<T>?, private val defaultVal: T? = null) {
    open operator fun getValue(thisRef: Any?, property: KProperty<*>): T = mirror?.get() ?: defaultVal ?: throw AssertionError("Unreachable state")
    open operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        if(mirror is KMutableProperty0){
            mirror.set(value)
        }
    }
}
@Suppress("unused")
open class NullableMirror<T>(private val mirror: KProperty0<T?>?, private val defaultVal: T? = null) {
    open operator fun getValue(thisRef: Any?, property: KProperty<*>): T? = mirror?.get() ?: defaultVal
    open operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T?) {
        if(mirror is KMutableProperty0){
            mirror.set(value)
        }
    }
}
