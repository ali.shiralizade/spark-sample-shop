package nz.co.spark.sampleshop.fragments.splash

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import nz.co.spark.sampleshop.data.model.Resource
import nz.co.spark.sampleshop.data.response.SessionResponse
import nz.co.spark.sampleshop.di.inject
import nz.co.spark.sampleshop.repository.UserRepository
import nz.co.spark.sampleshop.util.SingleLiveEvent
import nz.co.spark.sampleshop.viewmodel.BaseViewModel

class SplashFragmentVM : BaseViewModel(){


    val requestSession = SingleLiveEvent<Resource<SessionResponse>>()

    fun requestSession(){
        viewModelScope.launch {
            requestSession.value = Resource.loading()
            requestSession.value = UserRepository.requestSession()
        }
    }

}