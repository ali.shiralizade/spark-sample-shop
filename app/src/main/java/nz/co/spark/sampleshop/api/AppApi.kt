package nz.co.spark.sampleshop.api

import nz.co.spark.sampleshop.data.response.Category
import nz.co.spark.sampleshop.data.response.Product
import nz.co.spark.sampleshop.data.response.SessionResponse
import retrofit2.http.GET
import retrofit2.http.Query


interface AppApi {

    @GET("index.php?route=feed/rest_api/session")
    suspend fun requestSession(): SessionResponse

    @GET("index.php?route=androidcart/rest_api/categories")
    suspend fun getProductCategories(): ArrayList<Category>

    @GET("index.php?route=feed/rest_api/products&subcategory=true&only_special=false")
    suspend fun getProductList(
        @Query("category") category: String,
        @Query("limit") limit: Int,
        @Query("page") page: Int,
        @Query("sort") sort: String = "p.sort",
        @Query("order") order: String = "ASC"
    ): ArrayList<Product>

}




