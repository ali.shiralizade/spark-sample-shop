package nz.co.spark.sampleshop.fragments.splash

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import nz.co.spark.sampleshop.R
import nz.co.spark.sampleshop.databinding.FragmentSplashBinding
import nz.co.spark.sampleshop.fragments.BaseFragment
import nz.co.spark.sampleshop.fragments.observe
import nz.co.spark.sampleshop.util.NavAnimations


class SplashFragment : BaseFragment<FragmentSplashBinding, SplashFragmentVM>() {
    override val viewModel : SplashFragmentVM by viewModels()
    override fun layout() = R.layout.fragment_splash

    var waitJob : Job? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.requestSession()
        waitJob?.cancel()
        waitJob = launch {
            delay(500)
        }
    }

    override fun registerObservers() {

        observe(viewModel.requestSession){
            if (it.isLoading()){
                binding.showRetry = false
            }
            if (it.isSuccess()){
                binding.showRetry = false
                launch {
                    waitJob?.join()
                    waitJob = null
                    navigate(SplashFragmentDirections.navigateSplashToProductCategory())
                }
            }

            if (it.isError()){
                binding.showRetry = true
            }

        }

    }



}