package nz.co.spark.sampleshop.api

import nz.co.spark.sampleshop.data.model.Resource
import nz.co.spark.sampleshop.di.inject
import retrofit2.HttpException
import java.net.SocketTimeoutException

enum class ErrorCodes(val code: Int, val message: String) {
    SocketTimeOut(-1, "Timeout"),
    Unauthorized(401, "Unauthorized"),
    NotFound(404, "Not Found")
    ;

    companion object{
        fun fromCode(code: Int) = values().firstOrNull { it.code == code }
    }
}

object ResponseHandler {

    fun <T : Any> handleException(e: Exception): Resource<T> {
        return when (e) {
            is ApiFailedException -> Resource.error(e.message)
            is HttpException -> Resource.error(e)
            is SocketTimeoutException -> Resource.error(ErrorCodes.SocketTimeOut.message, null)
            else -> Resource.error("General Error", null)
        }
    }

    private fun getErrorMessage(code: Int) =
        ErrorCodes.fromCode(code)?.message ?: "General Error"
}

suspend fun <T : Any> apiCall(apiCal: suspend (AppApi.() -> T)): Resource<T> {
    return try {
        val api by inject<AppApi>()
        val response = api.apiCal()
        Resource.success(response)
    } catch (e: Exception) {
        return ResponseHandler.handleException(e)
    }
}
