package helpers

import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.IOException


fun jsonFile(filenameIn: String) : String {
    val file = File("sampleData/apiJson", if(filenameIn.endsWith(".json")) filenameIn else "$filenameIn.json")

    val text = StringBuilder()

    try {
        val br = BufferedReader(FileReader(file))
        var line: String?
        while (br.readLine().also { line = it } != null) {
            text.append(line)
            text.append('\n')
        }
        br.close()
    } catch (e: IOException) {
        e.printStackTrace()
    }

    return text.toString()
}