package helpers

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withContext

@ExperimentalCoroutinesApi
public suspend fun <T> LiveData<T>.await(skipFirstValue: Boolean = true): T {
    val firstVal = value
    var firstTime = true
    return withContext(Dispatchers.Main.immediate) {
        suspendCancellableCoroutine<T> { continuation ->
            val observer = object : Observer<T> {
                override fun onChanged(value: T) {
                    if(skipFirstValue){
                        if(firstTime && firstVal == value){
                            firstTime = false
                            return
                        }
                    }
                    continuation.resume(value){

                    }
                    removeObserver(this)
                }
            }

            observeForever(observer)

            continuation.invokeOnCancellation {
                removeObserver(observer)
            }
        }
    }
}