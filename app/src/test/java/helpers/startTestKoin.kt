package helpers

import com.google.gson.GsonBuilder
import nz.co.spark.sampleshop.api.AppApi
import nz.co.spark.sampleshop.api.AppRetrofitConverterFactory
import nz.co.spark.sampleshop.api.RetrofitInterceptor
import nz.co.spark.sampleshop.util.AppPreferences
import nz.co.spark.sampleshop.util.RemoteServicesUrl
import okhttp3.*
import org.koin.core.context.startKoin
import org.koin.dsl.KoinAppDeclaration
import org.koin.dsl.module
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

var didStartTestKoin = false
fun startTestKoin(appDeclaration: KoinAppDeclaration){
    if(didStartTestKoin) return
    didStartTestKoin = true
    startKoin {
        modules(listOf(
            module {
                single { AppPreferences(PrefsContextWrapper()) }
            },
            module {
                single { provideOkHttpClient(RetrofitInterceptor()) }
                single {
                    provideRetrofitInstance(
                        get(),
                        RemoteServicesUrl.BASE_URL
                    )
                }
                single { get<Retrofit>().create(AppApi::class.java) }
                single { GsonBuilder().create() }
            }
        ))
        appDeclaration()
    }
}


private fun provideRetrofitInstance(okHttpClient: OkHttpClient, baseUrl:String): Retrofit {
    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(okHttpClient)
        .addConverterFactory(AppRetrofitConverterFactory.create()).build()
}

private fun provideOkHttpClient(retrofitInterceptor: RetrofitInterceptor): OkHttpClient {
    val ans = OkHttpClient()
        .newBuilder()
        .addInterceptor(retrofitInterceptor)
        .addInterceptor(MockApiInterceptor)

    ans.connectTimeout(30 , TimeUnit.SECONDS)
    ans.readTimeout(30 , TimeUnit.SECONDS)

    return ans.build()
}

const val apiDelay = 200L
object MockApiInterceptor : Interceptor {
    // set to true, to get a failed answer (it will reset after one time use, so reset to true if you need it in another api call)
    var needsFailAns = false
    // of true, it will call a real http request, AKA interceptor will be dismissed
    var needsRealData = false

    override fun intercept(chain: Interceptor.Chain): Response {
        if(needsRealData){
            needsRealData = false
            return chain.proceed(chain.request())
        }
        val url = chain.request().url()
        val route = url.queryParameter("route") ?: ""

        val filename = if(needsFailAns) {
            needsFailAns = false
            "generalFailure.json"
        }else
            when (route){
                "feed/rest_api/session" -> "sessionSuccess.json"
                "androidcart/rest_api/categories" -> "getCategoriesSuccess.json"
                "feed/rest_api/products" -> {
                    url
                    ""
                }
                else -> ""
            }
        val responseString = jsonFile(filename)

        //simulate api call time
        Thread.sleep(apiDelay)

        return chain.proceed(chain.request())
            .newBuilder()
            .code(200)
            .protocol(Protocol.HTTP_2)
            .message(responseString)
            .body(ResponseBody.create(MediaType.parse("application/json"),
                responseString.toByteArray()))
            .addHeader("content-type", "application/json")
            .build()
    }
}

//private fun getJson