package helpers

import android.content.Context
import android.content.ContextWrapper
import android.content.SharedPreferences
import com.github.ivanshafran.sharedpreferencesmock.SPMockBuilder
import nz.co.spark.sampleshop.BuildConfig

class PrefsContextWrapper(val base: Context = SPMockBuilder().createContext()) : ContextWrapper(base) {
    override fun getSharedPreferences(name: String?, mode: Int): SharedPreferences {
        return base.getSharedPreferences(name, mode)
    }
    override fun getPackageName(): String {
        return BuildConfig.APPLICATION_ID
    }

    override fun deleteSharedPreferences(name: String?): Boolean {
        return base.deleteSharedPreferences(name)
    }
}