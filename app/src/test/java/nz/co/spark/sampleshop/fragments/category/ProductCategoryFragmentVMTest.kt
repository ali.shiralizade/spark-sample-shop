package nz.co.spark.sampleshop.fragments.category

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import helpers.await
import helpers.startTestKoin
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.test.KoinTest


@RunWith(JUnit4::class)
class ProductCategoryFragmentVMTest : KoinTest {

    @ExperimentalCoroutinesApi
    private val testDispatcher = TestCoroutineDispatcher()

    @ExperimentalCoroutinesApi
    @Before
    fun initTest() {
        startTestKoin {}
        Dispatchers.setMain(testDispatcher)
    }

    @ExperimentalCoroutinesApi
    @After
    fun endTest() {
        Dispatchers.resetMain()
    }

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @Test
    fun `fetch products`() = runBlocking {
        val vm = ProductCategoryFragmentVM()
        vm.getProductCategories() //ask to call api
        Assert.assertTrue(vm.categoriesResponse.value?.isLoading() == true)
        val categories = vm.categoriesResponse.await() //wait for success answer
        assert(categories.isSuccess())
        vm.getProductCategories()
        //ensure another api is not getting called (no loading)
        Assert.assertTrue(vm.categoriesResponse.value?.isSuccess() == true)
    }
}
