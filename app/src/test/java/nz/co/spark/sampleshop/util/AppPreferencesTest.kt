package nz.co.spark.sampleshop.util

import helpers.startTestKoin
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.KoinTest
import org.koin.test.get

class AppPreferencesTest : KoinTest {

    private lateinit var prefs : AppPreferences

    @Before
    fun initTest() {
        startTestKoin {}

        prefs = get()
    }

    @Test
    fun `test app preferences token set and get`() {
        prefs.session = "test"
        Assert.assertEquals(prefs.session , "test")
    }
}