package nz.co.spark.sampleshop.fragments.splash

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import helpers.MockApiInterceptor
import helpers.await
import helpers.startTestKoin
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.test.KoinTest


@RunWith(JUnit4::class)
class SplashFragmentVMTest : KoinTest {

    @ExperimentalCoroutinesApi
    private val testDispatcher = TestCoroutineDispatcher()

    @ExperimentalCoroutinesApi
    @Before
    fun initTest() {
        startTestKoin {}
        Dispatchers.setMain(testDispatcher)
    }

    @ExperimentalCoroutinesApi
    @After
    fun endTest(){
        Dispatchers.resetMain()
    }

    @Rule
    @JvmField
    public var rule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @Test
    fun `fetch a session an listen to answer`() = runBlocking{
        val vm = SplashFragmentVM()
        vm.requestSession()
        Assert.assertTrue(vm.requestSession.value?.isLoading() ?: false)
        assert(vm.requestSession.await().isSuccess())
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `fetch a session an listen to fail answer`() = runBlocking{
        MockApiInterceptor.needsFailAns = true
        val vm = SplashFragmentVM()
        vm.requestSession()
        Assert.assertTrue(vm.requestSession.value?.isLoading() ?: false)
        assert(vm.requestSession.await().isError())
    }
}