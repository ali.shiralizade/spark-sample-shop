package nz.co.spark.sampleshop.api

import helpers.MockApiInterceptor
import helpers.jsonFile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import nz.co.spark.sampleshop.data.response.SessionResponse
import helpers.startTestKoin
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.KoinTest
import org.koin.test.get
import java.io.File
import java.io.FileInputStream

class AppRetrofitConverterFactoryTest : KoinTest {

    @ExperimentalCoroutinesApi
    private val testDispatcher = TestCoroutineDispatcher()


    @ExperimentalCoroutinesApi
    @Before
    fun initTest() {
        startTestKoin { }
        Dispatchers.setMain(testDispatcher)
    }

    @ExperimentalCoroutinesApi
    @After
    fun endTest(){
        Dispatchers.resetMain()
    }

    @Test
    fun `test read json file`() {
        val sessionSuccess = jsonFile("sessionSuccess.json")
        Assert.assertTrue(sessionSuccess.length > 10)
    }

    @Test
    fun `test type adapter`() {
        val sessionJson = jsonFile("sessionSuccess.json")
        val factory = AppRetrofitConverterFactory.create(get())
        val converter = factory.responseBodyConverter(SessionResponse::class.java, arrayOf(), get())
        val sessionResponse = converter?.convert(ResponseBody.create(MediaType.get("application/json"), sessionJson)) as? SessionResponse
        Assert.assertEquals("77pnpu9pb9oa5ka13iha3reuc5", sessionResponse?.session)
    }

    @ExperimentalCoroutinesApi
    @Test
    //This test now is not a rea one, it will be answered by MockInterceptor
    fun `test real session api`() = runBlocking{
        val sessionAns = apiCall { requestSession() }
        if(sessionAns.data?.session?.isEmpty() != false){
            Assert.fail()
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `test mocked session api`() = runBlocking{
        val sessionAns = apiCall { requestSession() }
        Assert.assertEquals("77pnpu9pb9oa5ka13iha3reuc5", sessionAns.data?.session)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `test mocked session error api`() = runBlocking{
        MockApiInterceptor.needsFailAns = true
        val sessionAns = apiCall { requestSession() }
        Assert.assertFalse(sessionAns.message.isNullOrBlank())
    }
}